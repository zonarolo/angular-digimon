import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DigimonsComponent } from './pages/digimons/digimons.component';
import { DigimonDetailComponent } from './pages/digimons/pages/digimon-detail/digimon-detail.component';
import { ContactComponent } from './pages/contact/contact.component';


const routes: Routes = [
  {
    path:'digimons', component: DigimonsComponent
  },
  {
    path: '', redirectTo: 'digimons', pathMatch: 'full'
  },
  {
    path: 'digimon/:digimonName', component: DigimonDetailComponent
  },
  {
    path: 'contact', component: ContactComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
