import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { DigimonsComponent } from './pages/digimons/digimons.component';
import { DigimonsListComponent } from './pages/digimons/components/digimons-list/digimons-list.component';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { DigimonsTableComponent } from './pages/digimons/components/digimons-table/digimons-table.component';
import { DigimonDetailComponent } from './pages/digimons/pages/digimon-detail/digimon-detail.component';
import { ContactComponent } from './pages/contact/contact.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DigimonsComponent,
    DigimonsListComponent,
    DigimonsTableComponent,
    DigimonDetailComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ButtonModule,
    TableModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
