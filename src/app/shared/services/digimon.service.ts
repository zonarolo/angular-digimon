import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class DigimonService {


  constructor(private httpClient: HttpClient) { }

  getAll(){
    return this.httpClient.get(environment.url);
  }
  getDigimon(digimonName){
    return this.httpClient.get(environment.url + "/name/" + digimonName);
  }
}
