import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-digimons-table',
  templateUrl: './digimons-table.component.html',
  styleUrls: ['./digimons-table.component.scss']
})
export class DigimonsTableComponent implements OnInit {


  @Input() digimonTable: Array<any>;

  constructor() { }

  ngOnInit(): void {
  }

}
