import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-digimons-list',
  templateUrl: './digimons-list.component.html',
  styleUrls: ['./digimons-list.component.scss']
})
export class DigimonsListComponent implements OnInit {


  @Input() digimonList: Array<any>;
  constructor() { }

  ngOnInit(): void {
  }

}
